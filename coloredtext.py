import colorama
from colorama import Fore,Back,Style
colorama.init(autoreset=True)

print(Fore.YELLOW + Back.RED + "  “   The purpose of our lives is to be happy.”  " )
print(Fore.BLACK+Back.WHITE+" In the end, its not the years in your life that count... ")

print(Fore.WHITE + Back.BLUE + "   You only live once, but if you do it right, once is enough  ")

print(Fore.BLACK + Back.GREEN + "Winning doesn't always mean being first...   ")
